%global cygwin_gcc_version 4.9.3

Name:      cygwin-clang
Version:   3.5.2
Release:   4%{?dist}
Summary:   Clang compiler for Cygwin toolchain

Group:     Development/Languages
License:   NCSA
URL:       http://clang.llvm.org

Source0:   http://www.llvm.org/releases/%{version}/llvm-%{version}.src.tar.xz
Source1:   http://www.llvm.org/releases/%{version}/cfe-%{version}.src.tar.xz
Patch0:    llvm-3.5.0-gcc49.patch
Patch1:    llvm-3.5.0-gcc5.patch

Patch101:  clang-3.5.1-cygwin-includes.patch
Patch102:  clang-3.5.1-cygwin64.patch
Patch103:  clang-3.5.1-cygwin-cxa-atexit.patch

BuildRequires: cygwin32-filesystem
BuildRequires: cygwin64-filesystem
BuildRequires: gcc-c++
BuildRequires: libffi-devel
BuildRequires: ncurses-devel
BuildRequires: perl
%if 0%{?fedora} || 0%{?rhel} >= 7
BuildRequires: perl-podlators
%endif
BuildRequires: python

%description
Clang C/C++/ObjC compiler for the Cygwin toolchains

%package -n cygwin32-clang
Summary:    Clang compiler for Cygwin32 toolchain
Group:      Development/Languages
Obsoletes:  %{name} < %{version}-%{release}
Provides:   %{name} = %{version}-%{release}
Requires:   cygwin32-binutils
Requires:   cygwin32-gcc = %{cygwin_gcc_version}
Requires:   cygwin32-gcc-c++ = %{cygwin_gcc_version}

%description -n cygwin32-clang
%{description}

%package -n cygwin64-clang
Summary:    Clang compiler for Cygwin64 toolchain
Group:      Development/Languages
Requires:   cygwin64-binutils
Requires:   cygwin64-gcc = %{cygwin_gcc_version}
Requires:   cygwin64-gcc-c++ = %{cygwin_gcc_version}

%description -n cygwin64-clang
%{description}


%prep
%setup -q -T -b 0 -a 1 -n llvm-%{version}.src
mv cfe-%{version}.src tools/clang
%patch0 -p1
%patch1 -p4
%patch101 -p2
%patch102 -p2
%patch103 -p2


%build
mkdir -p target_cyg32 target_cyg64
%define _configure ../configure

pushd target_cyg32
%{configure} --target=%{cygwin32_target} --enable-targets=host,cpp \
    --disable-assertions --enable-optimized --enable-debug-runtime \
    --enable-debug-symbols --enable-libffi \
    --disable-embed-stdcxx --disable-bindings --disable-shared \
    --with-gcc-toolchain=/usr --with-default-sysroot=%{cygwin32_sysroot} \
    --with-c-include-dirs=%{_prefix}/lib/gcc/%{cygwin32_target}/%{cygwin_gcc_version}/include:%{cygwin32_includedir}:%{cygwin32_includedir}/w32api \
    --with-clang-resource-dir=../lib/clang/%{cygwin32_target}/%{version}

make %{?_smp_mflags} clang-only REQUIRES_RTTI=1
make -C tools/clang/docs/tools man
popd

pushd target_cyg64
%{configure} --target=%{cygwin64_target} --enable-targets=host,cpp \
    --disable-assertions --enable-optimized --enable-debug-runtime \
    --enable-debug-symbols --enable-libffi \
    --disable-embed-stdcxx --disable-bindings --disable-shared \
    --with-gcc-toolchain=/usr --with-default-sysroot=%{cygwin64_sysroot} \
    --with-c-include-dirs=%{_prefix}/lib/gcc/%{cygwin64_target}/%{cygwin_gcc_version}/include:%{cygwin64_includedir}:%{cygwin64_includedir}/w32api \
    --with-clang-resource-dir=../lib/clang/%{cygwin64_target}/%{version}

make %{?_smp_mflags} clang-only REQUIRES_RTTI=1
make -C tools/clang/docs/tools man
popd


%install
pushd target_cyg32
make install -C tools/clang/lib/Headers \
    PROJ_headers=$RPM_BUILD_ROOT%{_prefix}/lib/clang/%{cygwin32_target}/%{version}/include

install -D -m0755 Release+Debug/bin/clang $RPM_BUILD_ROOT%{_bindir}/%{cygwin32_target}-clang
ln -s %{cygwin32_target}-clang $RPM_BUILD_ROOT%{_bindir}/%{cygwin32_target}-clang++
install -D -m0644 tools/clang/docs/tools/clang.1 $RPM_BUILD_ROOT%{_mandir}/man1/%{cygwin32_target}-clang.1
popd

pushd target_cyg64
make install -C tools/clang/lib/Headers \
    PROJ_headers=$RPM_BUILD_ROOT%{_prefix}/lib/clang/%{cygwin64_target}/%{version}/include

install -D -m0755 Release+Debug/bin/clang $RPM_BUILD_ROOT%{_bindir}/%{cygwin64_target}-clang
ln -s %{cygwin64_target}-clang $RPM_BUILD_ROOT%{_bindir}/%{cygwin64_target}-clang++
install -D -m0644 tools/clang/docs/tools/clang.1 $RPM_BUILD_ROOT%{_mandir}/man1/%{cygwin64_target}-clang.1
popd


%files -n cygwin32-clang
%doc LICENSE.TXT README.txt
%{_bindir}/%{cygwin32_target}-clang
%{_bindir}/%{cygwin32_target}-clang++
%{_prefix}/lib/clang/%{cygwin32_target}/
%{_mandir}/man1/%{cygwin32_target}-clang.*

%files -n cygwin64-clang
%doc LICENSE.TXT README.txt
%{_bindir}/%{cygwin64_target}-clang
%{_bindir}/%{cygwin64_target}-clang++
%{_prefix}/lib/clang/%{cygwin64_target}/
%{_mandir}/man1/%{cygwin64_target}-clang.*


%changelog
* Fri Aug 14 2015 Yaakov Selkowitz <yselkowi@redhat.com> - 3.5.2-4
- rebuilt for cygwin-gcc 4.9.3

* Mon Jun 22 2015 Yaakov Selkowitz <yselkowi@redhat.com> - 3.5.2-3
- Fix BRs on EL6

* Fri Jun 19 2015 Yaakov Selkowitz <yselkowi@redhat.com> - 3.5.2-2
- Fix FTBFS on GCC 5

* Fri Jun 19 2015 Yaakov Selkowitz <yselkowi@redhat.com> - 3.5.2-1
- new version, build for cygwin-gcc 4.9.2

* Sun Nov 02 2014 Yaakov Selkowitz <yselkowi@redhat.com> - 3.4.2-2
- Enabled -fuse-cxa-atexit flag by default

* Wed Jul 30 2014 Yaakov Selkowitz <yselkowitz@cygwin.com> - 3.4.2-1
- Version bump
- Add cygwin64 compiler
- Cleanup spec

* Mon Jan 14 2013 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 3.1-2
- Add patches for compatibility with w32api headers and libtool.

* Fri Jul 13 2012 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 3.1-1
- Version bump.
- Add patch to support dllexport/dllimport attributes.

* Sun Feb 05 2012 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 3.0-1
- Initial RPM release.
